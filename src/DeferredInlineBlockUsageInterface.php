<?php

namespace Drupal\komponent;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for deferred tracking of inline block usage.
 */
interface DeferredInlineBlockUsageInterface {

  /**
   * Defer a usage to be stored post create.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to add usage to.
   * @param string $block_id
   *   The block id.
   */
  public function deferUsage(EntityInterface $entity, string $block_id): void;

  /**
   * Store deferred usages.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to add usage to.
   */
  public function addDeferredUsage(EntityInterface $entity): void;

}
