<?php

namespace Drupal\komponent;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\komponent\EventSubscriber\PrepareLayout;
use Drupal\komponent\EventSubscriber\SetInlineBlockDependency;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the layout_builder prepare_layout event subscriber.
 */
class KomponentServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('layout_builder.element.prepare_layout')) {
      $definition = $container->getDefinition('layout_builder.element.prepare_layout');
      $definition->setClass(PrepareLayout::class);
    }

    $modules = $container->getParameter('container.modules');
    if (isset($modules['block_content'])) {
      $definition = new Definition(SetInlineBlockDependency::class);
      $definition->setArguments([
        new Reference('entity.repository'),
        new Reference('database'),
        new Reference('inline_block.usage'),
        new Reference('plugin.manager.layout_builder.section_storage'),
        new Reference('current_route_match'),
      ]);
      $definition->addTag('event_subscriber');
      $definition->setPublic(TRUE);
      $container->setDefinition('komponent.get_block_dependency_subscriber', $definition);
    }
  }

}
