<?php

namespace Drupal\komponent\EventSubscriber;

use Drupal\komponent\CloneLayoutBuilderSections;
use Drupal\komponent\Plugin\SectionStorage\FieldSectionStorage;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\layout_builder\Event\PrepareLayoutEvent;
use Drupal\layout_builder\EventSubscriber\PrepareLayout as CorePrepareLayout;

/**
 * {@inheritdoc}
 */
class PrepareLayout extends CorePrepareLayout {

  /**
   * {@inheritdoc}
   */
  public function onPrepareLayout(PrepareLayoutEvent $event) {
    $section_storage = $event->getSectionStorage();

    if (!$section_storage instanceof FieldSectionStorage) {
      return parent::onPrepareLayout($event);
    }
    $entity = $section_storage->getEntity();
    if (!$entity->isNew() && $entity instanceof TranslatableInterface && $entity->isNewTranslation()) {
      $this->layoutTempstoreRepository->delete($section_storage);
    }

    if (!$this->layoutTempstoreRepository->has($section_storage)) {
      $entity = $section_storage->getEntity();

      if (!$entity->isNew() && $entity instanceof TranslatableInterface && $entity->isNewTranslation()) {
        $sections = (new CloneLayoutBuilderSections())->clone($section_storage->getSections(), $entity->language());

        $section_storage->removeAllSections();

        foreach ($sections as $section) {
          $section_storage->appendSection($section);
        }
      }
      elseif ($entity->isNew()) {
        $sections = $section_storage->getDefaultSectionStorage()->getSections();
        foreach ($sections as $section) {
          $section_storage->appendSection($section);
        }
      }

      // Add storage to tempstore regardless of what the storage is.
      $this->layoutTempstoreRepository->set($section_storage);

    }
  }

}
