<?php

namespace Drupal\komponent;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Loosely based on Drupal\layout_builder\InlineBlockEntityOperations.
 */
final class InlineBlockEntityOperations implements ContainerInjectionInterface {

  /**
   * Entity field manager service.
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * Section storage manager service.
   */
  private SectionStorageManagerInterface $sectionStorageManager;

  /**
   * Inline block usage service.
   */
  private DecoratingInlineBlockUsage $inlineBlockUsage;

  /**
   * Entity type manager service.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * A logger channel.
   */
  private LoggerChannelInterface $logger;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface $section_storage_manager
   *   The section storage manager service.
   * @param \Drupal\komponent\DeferredInlineBlockUsageInterface $inline_block_usage
   *   The inline block usage service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   A logger channel.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, SectionStorageManagerInterface $section_storage_manager, DeferredInlineBlockUsageInterface $inline_block_usage, EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger_channel) {
    $this->entityFieldManager = $entity_field_manager;
    $this->sectionStorageManager = $section_storage_manager;
    $this->inlineBlockUsage = $inline_block_usage;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_channel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('inline_block.usage'),
      $container->get('entity_type.manager'),
      $container->get('logger.channel.komponent')
    );
  }

  /**
   * Add inline block usage to an entity after being saved.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   */
  public function handlePostSave(EntityInterface $entity): void {
    $this->inlineBlockUsage->addDeferredUsage($entity);
  }

  /**
   * Save block_content entities on entity during creation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   */
  public function handlePreSave(EntityInterface $entity): void {
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }

    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());

    $komponent_field_definitions = array_filter($field_definitions, function (FieldDefinitionInterface $field_definition) {
      return $field_definition->getType() === 'komponent';
    });

    if (empty($komponent_field_definitions)) {
      return;
    }

    foreach ($komponent_field_definitions as $komponent_field_definition) {
      $field_config = $komponent_field_definition->getConfig($entity->bundle());

      $sections = $this->getSections($entity, $field_config);
      $inline_block_components = $this->getInlineBlockComponents($sections);

      foreach ($inline_block_components as $inline_block_component) {
        // Duplicate the blocks if the entity is new.
        $this->saveInlineBlockComponent($entity, $inline_block_component, $entity->isNew());
      }

      // @todo Implement removeUnusedForEntityOnSave().
    }
  }

  /**
   * Gets an array of sections from a field.
   *
   * @return \Drupal\layout_builder\Section[]
   *   Sections array.
   */
  private function getSections(EntityInterface $entity, FieldConfigInterface $field_config) {
    $section_storage = $this->getSectionStorage($entity, $field_config);
    return $section_storage ? $section_storage->getSections() : [];
  }

  /**
   * Get the matching section storage.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Field\FieldConfigInterface $field_config
   *   The field configuration.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The matching section storage or null if none matched.
   */
  private function getSectionStorage(EntityInterface $entity, FieldConfigInterface $field_config): ?SectionStorageInterface {
    $contexts['entity'] = EntityContext::fromEntity($entity);
    $contexts['field'] = EntityContext::fromEntity($field_config);

    return $this->sectionStorageManager->findByContext($contexts, new CacheableMetadata());
  }

  /**
   * Gets section components using inline blocks from an array of sections.
   *
   * @param \Drupal\layout_builder\Section[] $sections
   *   The sections.
   *
   * @return \Drupal\layout_builder\SectionComponent[]
   *   An array of section components.
   */
  private function getInlineBlockComponents(array $sections): array {
    $inline_block_components = [];
    foreach ($sections as $section) {
      foreach ($section->getComponents() as $component) {
        $plugin = $component->getPlugin();
        if ($plugin instanceof DerivativeInspectionInterface && $plugin->getBaseId() === 'inline_block') {
          $inline_block_components[] = $component;
        }
      }
    }
    return $inline_block_components;
  }

  /**
   * Save inline block components.
   *
   * @todo figure out revision
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The host entity.
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The section component.
   * @param bool $duplicate_blocks
   *   Whether to duplicate the "block_content" entity.
   */
  protected function saveInlineBlockComponent(EntityInterface $entity, SectionComponent $component, bool $duplicate_blocks): void {
    /** @var \Drupal\layout_builder\Plugin\Block\InlineBlock $plugin */
    $plugin = $component->getPlugin();
    $pre_save_configuration = $plugin->getConfiguration();
    $plugin->saveBlockContent(TRUE, $duplicate_blocks);
    $post_save_configuration = $plugin->getConfiguration();

    if ((empty($pre_save_configuration['block_revision_id']) && !empty($post_save_configuration['block_revision_id']))) {

      $block_id = $this->getBlockIdFromRevisionId($post_save_configuration['block_revision_id']);
      if (!$block_id) {
        $this->logger->error('Failed getting block id from revision id %id', ['%id' => $post_save_configuration['block_revision_id']]);
      }
      else {
        if ($entity->isNew()) {
          $this->inlineBlockUsage->deferUsage($entity, $block_id);
        }
        else {
          $this->inlineBlockUsage->addUsage($block_id, $entity);
        }
      }
    }

    $component->setConfiguration($post_save_configuration);
  }

  /**
   * Gets a block id from a revision id.
   *
   * @param string $revision_id
   *   The block_content revision id.
   *
   * @return string|false
   *   A block id, false if none found.
   */
  private function getBlockIdFromRevisionId(string $revision_id) {
    $query = $this->entityTypeManager->getStorage('block_content')->getQuery()
      ->condition('revision_id', $revision_id);

    $result = $query->execute();

    return reset($result);
  }

}
