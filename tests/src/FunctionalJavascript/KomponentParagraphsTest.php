<?php

namespace Drupal\Tests\komponent\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Tests the Layout Builder UI.
 *
 * @group komponent
 */
class KomponentParagraphsTest extends WebDriverTestBase {

  use ContextualLinkClickTrait;
  use ParagraphsTestBaseTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'field_ui',
    'layout_builder',
    'layout_test',
    'node',
    'komponent',
    'language',
    'content_translation',
    'entity_reference_revisions',
    'paragraphs',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    $bundle = BlockContentType::create([
      'id' => 'basic',
      'label' => 'Basic block',
    ]);
    $bundle->save();

    $this->addParagraphsType('text');
    $this->addFieldtoParagraphType('text', 'field_text', 'text');
    $this->addParagraphsField($bundle->id(), 'field_paragraphs', 'block_content');

    $this->createContentType(['type' => 'bundle_with_section_field']);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_components',
      'entity_type' => 'node',
      'type' => 'komponent',
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_name' => 'field_components',
      'entity_type' => 'node',
      'bundle' => 'bundle_with_section_field',
      'label' => 'Components',
    ]);
    $field->save();

    // Assign widget settings for the default form mode.
    $display_repository->getFormDisplay('node', 'bundle_with_section_field')
      ->setComponent('field_components', [
        'type' => 'komponent_widget',
      ])
      ->save();

    // Assign display settings for default view mode.
    $display_repository->getViewDisplay('node', 'bundle_with_section_field')
      ->setComponent('field_components', [
        'label' => 'hidden',
        'type' => 'komponent_formatter',
      ])
      ->save();

    ConfigurableLanguage::create(['id' => 'sv'])->save();
    $this->contentTranslationManager = $this->container->get('content_translation.manager');
  }

  /**
   * Tests creating a node with paragraphs in the components field.
   */
  public function testParagraphs() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
    ], 'foobar'));

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_text][0][value]"]');

    $page->fillField('settings[label]', 'Inline block title');

    $this->assertNotEmpty($textfield);
    $textfield->setValue('First paragraph');
    $page->pressButton('Add text');

    $textfield_2 = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_text][0][value]"]');
    $textfield_2->setValue('Second paragraph');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');

    $page->pressButton('Save');
    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');
  }

  /**
   * Tests translating paragraphs.
   */
  public function testTranslateParagraphs() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'translate any entity',
      'create content translations',
    ], 'foobar'));

    $this->contentTranslationManager->setEnabled('node', 'bundle_with_section_field', TRUE);

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_text][0][value]"]');

    $page->fillField('settings[label]', 'Inline block title');

    $this->assertNotEmpty($textfield);
    $textfield->setValue('First paragraph');
    $page->pressButton('Add text');

    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_text][0][value]"]');
    $textfield->setValue('Second paragraph');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');

    $page->pressButton('Save');
    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');

    $this->drupalGet('node/1/translations/add/en/sv');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('First paragraph', $textfield->getValue());
    $textfield->setValue('First paragraph SV');

    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertSame('Second paragraph', $textfield->getValue());
    $textfield->setValue('Second paragraph SV');

    $page->pressButton('Add text');
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][2][subform][field_text][0][value]"]');
    $textfield->setValue('Third paragraph SV');

    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('First paragraph SV');
    $assert_session->waitForText('Second paragraph SV');
    $assert_session->waitForText('Third paragraph SV');

    $page->pressButton('Save');

    $this->drupalGet('sv/node/1');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('First paragraph SV');
    $assert_session->pageTextContains('Second paragraph SV');
    $assert_session->pageTextContains('Third paragraph SV');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');
    $assert_session->pageTextNotContains('Third paragraph SV');

  }

  /**
   * Tests default values using paragraphs.
   */
  public function testDefaultValue() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'administer node fields',
    ], 'foobar'));

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('admin/structure/types/manage/bundle_with_section_field/fields/node.bundle_with_section_field.field_components');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_text][0][value]"]');
    $page->fillField('settings[label]', 'Inline block title');
    $this->assertNotEmpty($textfield);
    $textfield->setValue('First paragraph');

    $page->pressButton('Add text');
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_text][0][value]"]');
    $textfield->setValue('Second paragraph');

    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $page->pressButton('Save settings');

    $this->drupalGet('node/add/bundle_with_section_field');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('First paragraph');
    $assert_session->pageTextContains('Second paragraph');

    $page->fillField('title[0][value]', 'Test');

    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('First paragraph', $textfield->getValue());
    $textfield->setValue('First paragraph modified');
    $page->pressButton('Add text');
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][2][subform][field_text][0][value]"]');
    $textfield->setValue('Third paragraph');

    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('First paragraph modified');
    $assert_session->waitForText('Second paragraph');
    $assert_session->waitForText('Third paragraph');

    $page->pressButton('Save');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('First paragraph modified');
    $assert_session->pageTextContains('Second paragraph');
    $assert_session->pageTextContains('Third paragraph');

    $this->drupalGet('node/add/bundle_with_section_field');
    $assert_session->pageTextNotContains('First paragraph modified');
    $assert_session->pageTextNotContains('Third modified');
  }

  /**
   * Tests creating and translating nested paragraphs.
   */
  public function testNestedParagraphs() {
    $this->contentTranslationManager->setEnabled('node', 'bundle_with_section_field', TRUE);
    $this->addParagraphsType('nested_paragraph');
    $this->addFieldtoParagraphType('nested_paragraph', 'field_nested_paragraphs', 'entity_reference_revisions', ['target_type' => 'paragraph']);

    $field = FieldConfig::loadByName('block_content', 'basic', 'field_paragraphs');
    $field->setSetting('handler_settings', ['target_bundles' => ['nested_paragraph' => 'nested_paragraph']]);
    $field->save();

    $field = FieldConfig::loadByName('paragraph', 'nested_paragraph', 'field_nested_paragraphs');
    $field->setSetting('handler_settings', ['target_bundles' => ['text' => 'text']]);
    $field->save();
    $field_storage = FieldStorageConfig::loadByName('paragraph', 'field_nested_paragraphs');
    $field_storage->setCardinality(-1);
    $field_storage->save();

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('paragraph', 'nested_paragraph');
    $form_display->setComponent('field_nested_paragraphs', [
      'type' => 'paragraphs',
      'settings' => [
        'title' => 'Paragraph',
        'title_plural' => 'Paragraphs',
        'edit_mode' => 'open',
        'closed_mode' => 'summary',
        'autocollapse' => 'none',
        'add_mode' => 'button',
        'form_display_mode' => 'default',
        'default_paragraph_type' => '_none',
        'features' => [
          'duplicate' => 'duplicate',
          'collapse_edit_all' => 'collapse_edit_all',
          'add_above' => 'add_above',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
    ])
      ->save();

    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'translate any entity',
      'create content translations',
    ], 'foobar'));

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    $page->fillField('settings[label]', 'Inline block title');

    $paragraph_one_add_text_selector = 'settings_block_form_field_paragraphs_0_subform_field_nested_paragraphs_text_add_more';
    $paragraph_two_add_text_selector = 'settings_block_form_field_paragraphs_1_subform_field_nested_paragraphs_text_add_more';
    $page->pressButton($paragraph_one_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertNotEmpty($textfield);
    $textfield->setValue('Paragraph 1: Text 1');

    $page->pressButton($paragraph_one_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertNotEmpty($textfield);
    $textfield->setValue('Paragraph 1: Text 2');

    $page->pressButton('Add nested_paragraph');
    $assert_session->assertWaitOnAjaxRequest();

    $page->pressButton($paragraph_two_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $textfield->setValue('Paragraph 2: Text 1');

    $page->pressButton($paragraph_two_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $textfield->setValue('Paragraph 2: Text 2');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('Paragraph 1: Text 1');
    $assert_session->pageTextContains('Paragraph 1: Text 2');
    $assert_session->pageTextContains('Paragraph 2: Text 1');
    $assert_session->pageTextContains('Paragraph 2: Text 2');

    $page->pressButton('Save');
    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('Paragraph 1: Text 1');
    $assert_session->pageTextContains('Paragraph 1: Text 2');
    $assert_session->pageTextContains('Paragraph 2: Text 1');
    $assert_session->pageTextContains('Paragraph 2: Text 2');

    $this->drupalGet('node/1/translations/add/en/sv');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 1: Text 1', $textfield->getValue());
    $textfield->setValue('Paragraph 1: Text 1 - SV');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 1: Text 2', $textfield->getValue());
    $textfield->setValue('Paragraph 1: Text 2 - SV');

    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 2: Text 1', $textfield->getValue());
    $textfield->setValue('Paragraph 2: Text 1 - SV');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 2: Text 2', $textfield->getValue());
    $textfield->setValue('Paragraph 2: Text 2 - SV');

    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('Paragraph 1: Text 1 - SV');
    $assert_session->waitForText('Paragraph 1: Text 2 - SV');
    $assert_session->waitForText('Paragraph 2: Text 1 - SV');
    $assert_session->waitForText('Paragraph 2: Text 2 - SV');

    $page->pressButton('Save');

    $this->drupalGet('sv/node/1');

    $assert_session->pageTextContains('Paragraph 1: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 1: Text 2 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 2 - SV');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Paragraph 1: Text 1');
    $assert_session->pageTextContains('Paragraph 1: Text 2');
    $assert_session->pageTextContains('Paragraph 2: Text 1');
    $assert_session->pageTextContains('Paragraph 2: Text 2');

  }

  /**
   * Tests translating nested paragraphs from non-default language.
   */
  public function testNestedNonDefaultLanguageParagraphs() {
    $this->contentTranslationManager->setEnabled('node', 'bundle_with_section_field', TRUE);
    $config = ContentLanguageSettings::loadByEntityTypeBundle('node', 'bundle_with_section_field');
    $config->setDefaultLangcode('current_interface')->save();

    $this->addParagraphsType('nested_paragraph');
    $this->addFieldtoParagraphType('nested_paragraph', 'field_nested_paragraphs', 'entity_reference_revisions', ['target_type' => 'paragraph']);

    $field = FieldConfig::loadByName('block_content', 'basic', 'field_paragraphs');
    $field->setSetting('handler_settings', ['target_bundles' => ['nested_paragraph' => 'nested_paragraph']]);
    $field->save();

    $field = FieldConfig::loadByName('paragraph', 'nested_paragraph', 'field_nested_paragraphs');
    $field->setSetting('handler_settings', ['target_bundles' => ['text' => 'text']]);
    $field->save();
    $field_storage = FieldStorageConfig::loadByName('paragraph', 'field_nested_paragraphs');
    $field_storage->setCardinality(-1);
    $field_storage->save();

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('paragraph', 'nested_paragraph');
    $form_display->setComponent('field_nested_paragraphs', [
      'type' => 'paragraphs',
      'settings' => [
        'title' => 'Paragraph',
        'title_plural' => 'Paragraphs',
        'edit_mode' => 'open',
        'closed_mode' => 'summary',
        'autocollapse' => 'none',
        'add_mode' => 'button',
        'form_display_mode' => 'default',
        'default_paragraph_type' => '_none',
        'features' => [
          'duplicate' => 'duplicate',
          'collapse_edit_all' => 'collapse_edit_all',
          'add_above' => 'add_above',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
    ])
      ->save();

    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'translate any entity',
      'create content translations',
    ], 'foobar'));

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $language_negotiator = \Drupal::getContainer()->get('language_negotiator');
    // Set Interface Language Negotiator to Session.
    $language_negotiator->saveConfiguration('language_interface', [
      'language-url' => 1,
      'language-selected' => 2,
    ]);

    $this->config('language.negotiation')->set('url', [
      'source' => 'path_prefix',
      'prefixes' => [
        'en' => '',
        'sv' => 'sv',
      ],
    ])->save();

    $this->drupalGet('sv/node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test SV');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();

    $page->fillField('settings[label]', 'Inline block title');

    $paragraph_one_add_text_selector = 'settings_block_form_field_paragraphs_0_subform_field_nested_paragraphs_text_add_more';
    $paragraph_two_add_text_selector = 'settings_block_form_field_paragraphs_1_subform_field_nested_paragraphs_text_add_more';
    $page->pressButton($paragraph_one_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertNotEmpty($textfield);
    $textfield->setValue('Paragraph 1: Text 1 - SV');

    $page->pressButton($paragraph_one_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertNotEmpty($textfield);
    $textfield->setValue('Paragraph 1: Text 2 - SV');

    $page->pressButton('Add nested_paragraph');
    $assert_session->assertWaitOnAjaxRequest();

    $page->pressButton($paragraph_two_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $textfield->setValue('Paragraph 2: Text 1 - SV');

    $page->pressButton($paragraph_two_add_text_selector);
    $assert_session->assertWaitOnAjaxRequest();
    $textfield = $assert_session->waitForElement('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $textfield->setValue('Paragraph 2: Text 2 - SV');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('Paragraph 1: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 1: Text 2 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 2 - SV');

    $page->pressButton('Save');
    $this->drupalGet('sv/node/1');

    $assert_session->pageTextContains('Inline block title');
    $assert_session->pageTextContains('Paragraph 1: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 1: Text 2 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 2 - SV');

    $this->drupalGet('node/1/translations/add/sv/en');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 1: Text 1 - SV', $textfield->getValue());
    $textfield->setValue('Paragraph 1: Text 1 - EN');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][0][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 1: Text 2 - SV', $textfield->getValue());
    $textfield->setValue('Paragraph 1: Text 2 - EN');

    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][0][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 2: Text 1 - SV', $textfield->getValue());
    $textfield->setValue('Paragraph 2: Text 1 - EN');
    $textfield = $assert_session->waitForElementVisible('css', '[name="settings[block_form][field_paragraphs][1][subform][field_nested_paragraphs][1][subform][field_text][0][value]"]');
    $this->assertSame('Paragraph 2: Text 2 - SV', $textfield->getValue());
    $textfield->setValue('Paragraph 2: Text 2 - EN');

    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('Paragraph 1: Text 1 - EN');
    $assert_session->waitForText('Paragraph 1: Text 2 - EN');
    $assert_session->waitForText('Paragraph 2: Text 1 - EN');
    $assert_session->waitForText('Paragraph 2: Text 2 - EN');

    $page->pressButton('Save');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Paragraph 1: Text 1 - EN');
    $assert_session->pageTextContains('Paragraph 1: Text 2 - EN');
    $assert_session->pageTextContains('Paragraph 2: Text 1 - EN');
    $assert_session->pageTextContains('Paragraph 2: Text 2 - EN');

    $this->drupalGet('sv/node/1');

    $assert_session->pageTextContains('Paragraph 1: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 1: Text 2 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 1 - SV');
    $assert_session->pageTextContains('Paragraph 2: Text 2 - SV');

  }

}
