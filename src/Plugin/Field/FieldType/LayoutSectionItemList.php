<?php

namespace Drupal\komponent\Plugin\Field\FieldType;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\layout_builder\Field\LayoutSectionItemList as CoreLayoutSectionItemList;
use Drupal\layout_builder\Section;

/**
 * {@inheritdoc}
 */
class LayoutSectionItemList extends CoreLayoutSectionItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultAccess($operation = 'view', AccountInterface $account = NULL) {
    return AccessResult::allowed();
  }

  /**
   * Gets the matching section storage.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface
   *   Either the version of this section storage from tempstore, or the passed
   *   section storage if none exists.
   */
  protected function getSectionStorage() {
    $host_entity = $this->getEntity();
    $context = [
      'entity_type' => new Context(new ContextDefinition('string'), $host_entity->getEntityTypeId()),
      'bundle' => new Context(new ContextDefinition('string'), $host_entity->bundle()),
      'field' => EntityContext::fromEntity($this->getFieldDefinition()),
    ];
    $section_storage_manager = \Drupal::service('plugin.manager.layout_builder.section_storage');
    $section_storage = $section_storage_manager->load('field_defaults', $context);
    /** @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $tempstore_manager */
    $tempstore_manager = \Drupal::service('layout_builder.tempstore_repository');
    return $tempstore_manager->get($section_storage);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {
    $section_storage = $this->getSectionStorage();
    $tempstore = \Drupal::service('layout_builder.tempstore_repository');
    $temp = $tempstore->get($section_storage);
    // $form_state->set('section_storage', $temp);
    return [
      'build' => [
        '#type' => 'layout_builder',
        '#section_storage' => $temp,
        '#parents' => ['default_value_input'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesFormSubmit(array $element, array &$form, FormStateInterface $form_state) {
    $section_storage = $this->getSectionStorage();
    /** @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $tempstore_manager */
    $tempstore_manager = \Drupal::service('layout_builder.tempstore_repository');
    $tempstore_manager->delete($section_storage);
    return array_map(fn(Section $value) => $value->toArray(), $section_storage->getSections());
  }

}
