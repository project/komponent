<?php

namespace Drupal\komponent\Plugin\SectionStorage;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Plugin\SectionStorage\SectionStorageBase;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines the 'Field' section storage type.
 *
 * @SectionStorage(
 *   id = "field",
 *   weight = -30,
 *   handles_permission_check = TRUE,
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity"),
 *     "field" = @ContextDefinition("entity:field_config"),
 *   }
 * )
 */
class FieldSectionStorage extends SectionStorageBase implements ContainerFactoryPluginInterface {

  use LayoutBuilderContextTrait;

  /**
   * Stores the shared tempstore.
   */
  protected SharedTempStore $tempStore;

  /**
   * The entity repository.
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The section storage manager.
   */
  protected SectionStorageManagerInterface $sectionStorageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SharedTempStoreFactory $temp_store_factory, EntityRepositoryInterface $entity_repository, SectionStorageManagerInterface $section_storage_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tempStore = $temp_store_factory->get('layout_builder.section_storage.field');
    $this->entityRepository = $entity_repository;
    $this->sectionStorageManager = $section_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.shared'),
      $container->get('entity.repository'),
      $container->get('plugin.manager.layout_builder.section_storage')
    );
  }

  /**
   * The storage id.
   */
  protected ?string $storageId = NULL;

  /**
   * Gets the entity storing the overrides.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The entity storing the overrides.
   */
  public function getEntity() {
    return $this->getContextValue('entity');
  }

  /**
   * {@inheritdoc}
   */
  protected function getSectionList() {
    return $this->getEntity()->get($this->getContextValue('field')->getName());
  }

  /**
   * Sets a new storage id.
   *
   * @param string $id
   *   Unique identifier for storage.
   */
  public function setStorageId(string $id) {
    $this->storageId = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageId() {
    $entity = $this->getEntity();

    if ($entity->isNew()) {
      if ($this->storageId) {
        return $this->storageId;
      }
      $key = $entity->getEntityTypeId() . '.' . $entity->uuid() . '.' . $this->getContextValue('field')->getName();
    }
    else {
      $key = $entity->getEntityTypeId() . '.' . $entity->id() . '.' . $this->getContextValue('field')->getName();
    }

    // @todo Allow entities to provide this contextual information in
    //   https://www.drupal.org/project/drupal/issues/3026957.
    if ($entity instanceof TranslatableInterface) {
      $key .= '.' . $entity->language()->getId();
    }

    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function getTempstoreKey() {
    return $this->getStorageId();
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutBuilderUrl($rel = 'view') {
    $entity = $this->getEntity();
    if ($entity->isNew()) {
      // @todo 'node_type' => bundle_key somehow.
      return Url::fromRoute("{$entity->getEntityTypeId()}.add", ['node_type' => $entity->bundle()]);
    }
    return $entity->toUrl('edit-form');
  }

  /**
   * Gets the section storage from tempstore.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $entity_id
   *   Unique identifier of entity.
   * @param string $field_name
   *   The field name.
   * @param string $langcode
   *   The language of the entity.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage loaded from tempstore.
   */
  protected function getSectionStorageFromTempStore(string $entity_type_id, string $entity_id, string $field_name, string $langcode) {
    $tempstore = $this->tempStore->get($entity_type_id . '.' . $entity_id . '.' . $field_name . '.' . $langcode);
    return !empty($tempstore['section_storage']) ? $tempstore['section_storage'] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deriveContextsFromRoute($value, $definition, $name, array $defaults) {
    [$entity_type_id, $entity_id, $field_name, $langcode] = explode('.', $value);
    $contexts = [];
    $section_storage = $this->getSectionStorageFromTempStore($entity_type_id, $entity_id, $field_name, $langcode);
    if ($section_storage && $entity = $section_storage->getEntity()) {
      $contexts['entity'] = EntityContext::fromEntity($entity);
      $field_config = $entity->{$field_name}->getFieldDefinition();
      $contexts['field'] = EntityContext::fromEntity($field_config);
    }

    if (empty($contexts)) {
      $entity = $this->entityRepository->getActive($entity_type_id, $entity_id);
      $contexts['entity'] = EntityContext::fromEntity($entity);
      $field_config = $entity->{$field_name}->getFieldDefinition();
      $contexts['field'] = EntityContext::fromEntity($field_config);
    }

    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  protected function handleTranslationAccess(AccessResult $result, $operation, AccountInterface $account) {
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl() {
    return $this->getEntity()->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function buildRoutes(RouteCollection $collection) {
    // @todo build routes.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getEntity()->label();
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    return $this->getEntity()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(RefinableCacheableDependencyInterface $cacheability) {
    // @todo does returning TRUE make sense?
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $return_as_object ? AccessResult::allowed() : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSectionStorage() {
    $context = [
      'entity_type' => new Context(new ContextDefinition('string'), $this->getEntity()->getEntityTypeId()),
      'bundle' => new Context(new ContextDefinition('string'), $this->getEntity()->bundle()),
      'field' => $this->getContext('field'),
    ];
    return $this->sectionStorageManager->load('field_defaults', $context);
  }

}
