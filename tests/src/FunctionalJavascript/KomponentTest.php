<?php

namespace Drupal\Tests\komponent\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\Tests\layout_builder\FunctionalJavascript\LayoutBuilderSortTrait;

/**
 * Tests the Layout Builder UI.
 *
 * @group komponent
 */
class KomponentTest extends WebDriverTestBase {

  use ContextualLinkClickTrait;
  use LayoutBuilderSortTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'field_ui',
    'layout_builder',
    'layout_test',
    'node',
    'komponent',
    'language',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $bundle = BlockContentType::create([
      'id' => 'basic',
      'label' => 'Basic',
    ]);
    $bundle->save();
    block_content_add_body_field($bundle->id());

    $this->createContentType(['type' => 'bundle_with_section_field']);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_components',
      'entity_type' => 'node',
      'type' => 'komponent',
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_name' => 'field_components',
      'entity_type' => 'node',
      'bundle' => 'bundle_with_section_field',
      'label' => 'Components',
    ]);
    $field->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Assign widget settings for the default form mode.
    $display_repository->getFormDisplay('node', 'bundle_with_section_field')
      ->setComponent('field_components', [
        'type' => 'komponent_widget',
      ])
      ->save();

    // Assign display settings for default view mode.
    $display_repository->getViewDisplay('node', 'bundle_with_section_field')
      ->setComponent('field_components', [
        'label' => 'hidden',
        'type' => 'komponent_formatter',
      ])
      ->save();

    ConfigurableLanguage::create(['id' => 'sv'])->save();
    $this->contentTranslationManager = $this->container->get('content_translation.manager');
  }

  /**
   * Tests the Components UI.
   */
  public function testComponents() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
    ], 'foobar'));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $page->fillField('settings[label]', 'Title');
    $textarea->setValue('Hello World');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('Hello World');

    $page->pressButton('Save');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Title');
    $assert_session->pageTextContains('Hello World');
  }

  /**
   * Tests that components are cleared when refreshing the page.
   */
  public function testComponentsAreCoupledToFormInstance() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
    ], 'foobar'));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $page->fillField('settings[label]', 'Block title');
    $textarea->setValue('Hello World');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Hello World');

    $this->drupalGet('node/add/bundle_with_section_field');

    $assert_session->pageTextNotContains('Block title');
    $assert_session->pageTextNotContains('Hello World');
  }

  /**
   * Tests adding and using default values.
   */
  public function testDefaultValue() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'administer node fields',
    ], 'foobar'));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('admin/structure/types/manage/bundle_with_section_field/fields/node.bundle_with_section_field.field_components');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $page->fillField('settings[label]', 'Block title');
    $textarea->setValue('Hello World');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');

    $page->pressButton('Save settings');

    $this->drupalGet('node/add/bundle_with_section_field');

    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Hello World');
  }

  /**
   * Tests editing components.
   */
  public function testEditComponents() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
    ], 'foobar'));

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');

    $page->fillField('title[0][value]', 'Test');

    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));

    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $page->fillField('settings[label]', 'Block title');
    $textarea->setValue('Hello World');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $page->pressButton('Save');
    $this->drupalGet('node/1');
    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Hello World');

    $this->drupalGet('node/1/edit');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textarea = $assert_session->waitForElementVisible('css', '[name="settings[block_form][body][0][value]"]');

    $this->assertSame('Hello World', $textarea->getValue());
    $textarea->setValue('Changed value');

    $page->pressButton('Update');
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('Changed value');

    $page->pressButton('Save');

    $this->drupalGet('node/1');

    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Changed value');
  }

  /**
   * Tests translating components.
   */
  public function testTranslateComponents() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'administer nodes',
      'bypass node access',
      'create and edit custom blocks',
      'translate any entity',
      'create content translations',
    ], 'foobar'));

    $this->contentTranslationManager->setEnabled('node', 'bundle_with_section_field', TRUE);

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('node/add/bundle_with_section_field');
    $page->fillField('title[0][value]', 'Test');
    $this->clickLink('Add section');
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      'Two column',
    ]));
    $this->clickLink('Two column');
    $assert_session->waitForElementVisible('named', ['button', 'Add section']);
    $page->pressButton('Add section');
    $assert_session->assertWaitOnAjaxRequest();

    $page->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForLink('Create custom block'));
    $this->clickLink('Create custom block');
    $assert_session->assertWaitOnAjaxRequest();
    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $page->fillField('settings[label]', 'Block title');
    $textarea->setValue('Hello World');
    $page->pressButton('Add block');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $page->pressButton('Save');
    $this->drupalGet('node/1');
    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Hello World');

    $this->drupalGet('node/1/translations/add/en/sv');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textarea = $assert_session->waitForElementVisible('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertSame('Hello World', $textarea->getValue());
    $textarea->setValue('Intermediate value');
    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('Intermediate value');

    $this->drupalGet('node/1/translations/add/en/sv');
    $assert_session->pageTextNotContains('Intermediate value');
    $this->clickContextualLink('.block-inline-blockbasic', 'Configure');
    $textarea = $assert_session->waitForElementVisible('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertSame('Hello World', $textarea->getValue());
    $textarea->setValue('Changed value');
    $page->pressButton('Update');

    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    $assert_session->waitForText('Changed value');

    $page->pressButton('Save');
    $this->drupalGet('sv/node/1');
    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Changed value');

    $this->drupalGet('node/1');
    $assert_session->pageTextContains('Block title');
    $assert_session->pageTextContains('Hello World');
  }

}
