<?php

namespace Drupal\komponent\Element;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\komponent\Plugin\SectionStorage\FieldSectionStorage;
use Drupal\layout_builder\Element\LayoutBuilder;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a render element for building the Layout Builder UI.
 */
class KomponentLayoutBuilder extends LayoutBuilder {

  /**
   * Indicates if section management should be hidden.
   */
  private bool $disableManageSections = FALSE;

  /**
   * Indicates whether a list of buttons should be shown instead of 'add block'.
   */
  private bool $useButtons = FALSE;

  /**
   * The block manager.
   */
  protected BlockManagerInterface $blockManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $event_dispatcher, BlockManagerInterface $block_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher);
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($element) {
    $section_storage = $element['#section_storage'];
    if (!$section_storage instanceof FieldSectionStorage) {
      return parent::preRender($element);
    }
    $field_config = $section_storage->getContextValue('field');
    $this->disableManageSections = (bool) $field_config->getSetting('disable_manage_sections');
    $this->useButtons = (bool) $field_config->getSetting('use_buttons');
    return parent::preRender($element);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildAddSectionLink(SectionStorageInterface $section_storage, $delta) {
    if ($this->disableManageSections) {
      return [];
    }
    return parent::buildAddSectionLink($section_storage, $delta);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildAdministrativeSection(SectionStorageInterface $section_storage, $delta) {
    $section = parent::buildAdministrativeSection($section_storage, $delta);
    $contexts = $section_storage->getContextsDuringPreview();
    if ($this->disableManageSections) {
      unset($section['remove'], $section['configure']);
    }
    if ($this->useButtons) {
      foreach (Element::children($section['layout-builder__section']) as $region) {
        $definitions = $this->blockManager->getFilteredDefinitions('komponent', $contexts, [
          'section_storage' => $section_storage,
          'region' => $region,
        ]);

        $links = [];
        foreach ($definitions as $block_id => $block) {
          $attributes = [
            'class' => [
              'use-ajax',
            ],
            'data-dialog-type' => 'dialog',
            'data-dialog-renderer' => 'off_canvas',
          ];
          $attributes['class'][] = 'js-layout-builder-block-link';
          $link = [
            'title' => $block['admin_label'],
            'url' => Url::fromRoute('layout_builder.add_block',
              [
                'section_storage_type' => $section_storage->getStorageType(),
                'section_storage' => $section_storage->getStorageId(),
                'delta' => $delta,
                'region' => $region,
                'plugin_id' => $block_id,
              ]
            ),
            'attributes' => $attributes,
          ];

          $links[] = $link;
        }

        $section['layout-builder__section'][$region]['layout_builder_add_block']['buttons'] = [
          '#type' => 'container',
        ];

        foreach ($links as $link) {
          $section['layout-builder__section'][$region]['layout_builder_add_block']['buttons'][] = [
            '#type' => 'link',
            '#url' => $link['url'],
            '#title' => $link['title'],
            '#attributes' => array_merge_recursive(
              $link['attributes'],
              ['class' => ['button', 'button--small']]
            ),
          ];
        }
        unset($section['layout-builder__section'][$region]['layout_builder_add_block']['link']);
      }
    }
    return $section;
  }

}
