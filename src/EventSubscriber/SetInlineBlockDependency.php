<?php

namespace Drupal\komponent\EventSubscriber;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\layout_builder\Access\LayoutPreviewAccessAllowed;
use Drupal\layout_builder\EventSubscriber\SetInlineBlockDependency as BaseSetInlineBlockDependency;

/**
 * An event subscriber that returns an access dependency for inline blocks.
 */
class SetInlineBlockDependency extends BaseSetInlineBlockDependency {

  /**
   * {@inheritdoc}
   */
  protected function getInlineBlockDependency(BlockContentInterface $block_content, string $operation) {
    $active_operations = ['update', 'delete'];
    $current_route = $this->currentRouteMatch->getRouteName();
    // @todo is there a better way to determine if the 'view' operation matches the active revision?
    if ($operation === 'view' && ($current_route && preg_match('entity\.[a-zA-Z_]+\.edit_form', $current_route))) {
      $active_operations[] = 'view';
    }

    $layout_entity_info = $this->usage->getUsage($block_content->id());
    if (empty($layout_entity_info) || empty($layout_entity_info->layout_entity_type) || empty($layout_entity_info->layout_entity_id)) {
      // If the block does not have usage information then we cannot set a
      // dependency. It may be used by another module besides layout builder.
      return NULL;
    }
    // When updating or deleting an inline block, resolve the inline block
    // dependency via the active revision, since it is the revision that should
    // be loaded for editing purposes.
    if (in_array($operation, $active_operations, TRUE)) {
      $layout_entity = $this->entityRepository->getActive($layout_entity_info->layout_entity_type, $layout_entity_info->layout_entity_id);
    }
    else {
      $layout_entity = $this->entityRepository->getCanonical($layout_entity_info->layout_entity_type, $layout_entity_info->layout_entity_id);
    }
    if ($this->isComponentsCompatibleEntity($layout_entity)) {
      if ($this->isBlockRevisionUsedInEntity($layout_entity, $block_content)) {
        return 'view' === $operation && $this->isAjax() ? new LayoutPreviewAccessAllowed() : $layout_entity;
      }

    }
    return NULL;
  }

  /**
   * Determines if an entity has komponent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   TRUE if the entity has a komponent field otherwise FALSE.
   */
  protected function isComponentsCompatibleEntity(EntityInterface $entity) {
    if (!$entity instanceof FieldableEntityInterface) {
      return FALSE;
    }

    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($field_definitions as $definition) {
      if ($definition->getFieldStorageDefinition()->getType() === 'komponent') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitySections(EntityInterface $entity) {
    $sections = [];

    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_definitions */
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());

    /** @var \Drupal\Core\Field\FieldDefinitionInterface $definition */
    foreach ($field_definitions as $definition) {
      if ($definition->getFieldStorageDefinition()->getType() === 'komponent') {
        $sections = [
          ...$sections,
          ...$entity->get($definition->getName())->getSections(),
        ];
      }
    }

    return $sections;
  }

}
