<?php

namespace Drupal\komponent\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Plugin\Field\FieldType\LayoutSectionItem;

/**
 * Plugin implementation of the 'komponent' field type.
 *
 * @internal
 *   Plugin classes are internal.
 *
 * @FieldType(
 *   id = "komponent",
 *   label = @Translation("Komponent"),
 *   description = @Translation("Komponent"),
 *   list_class = "\Drupal\komponent\Plugin\Field\FieldType\LayoutSectionItemList",
 *   cardinality = \Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
 *   default_formatter = "komponent_formatter",
 *   default_widget = "komponent_widget"
 * )
 *
 * @property \Drupal\layout_builder\Section section
 */
class KomponentItem extends LayoutSectionItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'disable_manage_sections' => 0,
      'use_buttons' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm($form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $element['disable_manage_sections'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('disable_manage_sections'),
      '#title' => $this->t('Disable the ability to manage sections (create, delete and edit)'),
    ];

    $element['use_buttons'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('use_buttons'),
      '#title' => $this->t('Use buttons'),
    ];

    return $element;
  }

}
