<?php

namespace Drupal\komponent;

use Drupal\Core\Entity\EntityInterface;
use Drupal\layout_builder\InlineBlockUsageInterface;

/**
 * Handles inline_block_usage for new entities.
 *
 * Inline block usage is stored in the database using ID. During node creation
 * we don't have an ID yet. This service stores inline block id:s per entity
 * keyed by UUID so they can be added to the inline block usage table when
 * the entity has an ID.
 */
class DecoratingInlineBlockUsage implements InlineBlockUsageInterface, DeferredInlineBlockUsageInterface {

  /**
   * An array of deferred usages, keyed by entity UUID.
   *
   * @phpstan-var array{string, string[]}
   */
  private array $deferred;

  /**
   * The inline block usage service.
   */
  private InlineBlockUsageInterface $decorated;

  /**
   * Class constructor.
   *
   * @param \Drupal\layout_builder\InlineBlockUsageInterface $decorated
   *   The class being decorated.
   */
  public function __construct(InlineBlockUsageInterface $decorated) {
    $this->decorated = $decorated;
    $this->deferred = [];
  }

  /**
   * Gets the deferred usages of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to load dependencies for.
   *
   * @return array
   *   Array of block_content entity IDs.
   */
  private function getDeferred(EntityInterface $entity): array {
    return $this->deferred[$entity->uuid()] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function deferUsage(EntityInterface $entity, string $block_id): void {
    $this->deferred[$entity->uuid()][] = $block_id;
  }

  /**
   * {@inheritdoc}
   */
  public function addDeferredUsage(EntityInterface $entity): void {
    foreach ($this->getDeferred($entity) as $block_id) {
      $this->decorated->addUsage($block_id, $entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addUsage($block_content_id, EntityInterface $entity) {
    $this->decorated->addUsage($block_content_id, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getUnused($limit = 100) {
    return $this->decorated->getUnused($limit);
  }

  /**
   * {@inheritdoc}
   */
  public function removeByLayoutEntity(EntityInterface $entity) {
    $this->decorated->removeByLayoutEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteUsage(array $block_content_ids) {
    $this->decorated->deleteUsage($block_content_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getUsage($block_content_id) {
    return $this->decorated->getUsage($block_content_id);
  }

}
