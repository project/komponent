<?php

namespace Drupal\komponent\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget to display the layout form on entity edit form.
 *
 * @FieldWidget(
 *   id = "komponent_widget",
 *   label = @Translation("Komponent Widget"),
 *   description = @Translation("A field widget for komponent."),
 *   field_types = {
 *     "komponent",
 *   },
 *   multiple_values = TRUE,
 * )
 */
class KomponentWidget extends WidgetBase {

  /**
   * The section storage manager.
   */
  protected SectionStorageManagerInterface $sectionStorageManager;

  /**
   * The layout tempstore repository.
   */
  protected LayoutTempstoreRepositoryInterface $layoutTempstoreRepository;

  /**
   * The renderer.
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SectionStorageManagerInterface $section_storage_manager, LayoutTempstoreRepositoryInterface $layout_tempstore_repository, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->sectionStorageManager = $section_storage_manager;
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getEntity();
    $field_definition = $items->getFieldDefinition();
    $field_name = $field_definition->getName();

    $user_input = $form_state->getUserInput();
    $storage_id = $user_input[$field_name]['storage_id'] ?? '';

    // Clear pending translation data when fully reloading the form.
    if ($entity instanceof TranslatableInterface && $entity->isNewTranslation()) {
      if (empty($storage_id)) {
        $section_storage = $this->getSectionStorageFromEntity($entity, $field_definition);
        if ($this->layoutTempstoreRepository->has($section_storage)) {
          $this->layoutTempstoreRepository->delete($section_storage);
        }
      }
    }

    $section_storage = $this->buildSectionStorage($entity, $field_definition, $storage_id);

    if (!$section_storage) {
      return NULL;
    }

    $element['builder'] = [
      '#type' => 'layout_builder',
      '#section_storage' => $section_storage,
    ];

    // Each time the page is refreshed or a new entity is submitted,
    // a new unique indicator (UUID) is created for the entity.
    // Storing UUID of an entity in a hidden element
    // prevents it from being lost from further processing.
    $element['storage_id'] = [
      '#type' => 'hidden',
      '#value' => $section_storage->getStorageId(),
    ];

    return $element;
  }

  /**
   * Build section storage, extracting storage from entity & tempstore.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param string $storage_id
   *   (optional) Unique identifier for storage.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage loaded from the entity or tempstore otherwise null.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   */
  public function buildSectionStorage(EntityInterface $entity, FieldDefinitionInterface $field_definition, string $storage_id = '') {
    $section_storage = $this->getSectionStorageFromEntity($entity, $field_definition);

    // Check if storage exist in tempstore.
    if ($section_storage) {
      return $this->getSectionStorageFromTempStore($section_storage, $storage_id);
    }

    return $section_storage;
  }

  /**
   * Gets the section storage from entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param \Drupal\Core\Field\FieldConfigInterface $field_config
   *   The field config.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage loaded from entity.
   */
  protected function getSectionStorageFromEntity(EntityInterface $entity, FieldConfigInterface $field_config) {
    return $this->sectionStorageManager->load('field', [
      'entity' => EntityContext::fromEntity($entity),
      'field' => EntityContext::fromEntity($field_config),
    ]);
  }

  /**
   * Gets the section storage from tempstore.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The entity to check.
   * @param string $storage_id
   *   (optional) Unique identifier for storage.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage loaded from tempstore.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   */
  protected function getSectionStorageFromTempStore(SectionStorageInterface $section_storage, string $storage_id = '') {
    $entity = $section_storage->getContextValue('entity');

    if ($storage_id && $entity->isNew()) {
      $this->setStorageId($section_storage, $storage_id);
    }

    if ($this->layoutTempstoreRepository->has($section_storage)) {
      $section_storage = $this->layoutTempstoreRepository->get($section_storage);
    }

    return $section_storage;
  }

  /**
   * Set storage id in section storage.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The entity to check.
   * @param string $storage_id
   *   Unique identifier for storage.
   */
  public function setStorageId(SectionStorageInterface $section_storage, string $storage_id) {
    $section_storage->setStorageId($storage_id);
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    if (!$form_state->isValidationComplete()) {
      return;
    }

    $entity = $items->getEntity();
    $field_config = $this->fieldDefinition->getConfig($entity->bundle());
    $field_name = $this->fieldDefinition->getName();

    $path = array_merge($form['#parents'], [$field_name, 'storage_id']);
    $key_exists = NULL;

    /** @var \Drupal\layout_builder\OverridesSectionStorageInterface $section_storage */
    $storage_id = NestedArray::getValue($form_state->getValues(), $path, $key_exists);
    if ($key_exists) {
      $section_storage = $this->getSectionStorageFromEntity($entity, $field_config);
      if ($section_storage) {
        $section_storage = $this->getSectionStorageFromTempStore($section_storage, $storage_id);

        $sections = $section_storage->getSections();

        $items->setValue($sections);
        // Deleting section storage from tempstore.
        $this->layoutTempstoreRepository->delete($section_storage);

        $this->messenger()
          ->addStatus($this->t('The layout override has been saved.'));
      }
    }
  }

}
