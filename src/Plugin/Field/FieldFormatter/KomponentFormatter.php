<?php

namespace Drupal\komponent\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;

/**
 * A formatter to display komponent (layout builder sections).
 *
 * @FieldFormatter(
 *   id = "komponent_formatter",
 *   label = @Translation("Komponent Formatter"),
 *   description = @Translation("A field formatter for komponent."),
 *   field_types = {
 *     "komponent",
 *   },
 *   multiple_values = TRUE,
 * )
 */
class KomponentFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $contexts = [];
    $contexts['layout_builder.entity'] = EntityContext::fromEntity($items->getEntity());
    $contexts['entity'] = EntityContext::fromEntity($items->getEntity());
    $contexts['view_mode'] = new Context(new ContextDefinition('string'), $this->viewMode);

    $element = [];
    /** @var \Drupal\layout_builder\Field\LayoutSectionItemList $items */
    foreach ($items->getSections() as $key => $section) {
      $element[$key] = $section->toRenderArray($contexts);
    }
    return $element;
  }

}
