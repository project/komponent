<?php

namespace Drupal\komponent\Plugin\SectionStorage;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\layout_builder\Plugin\SectionStorage\DefaultsSectionStorage;
use Drupal\layout_builder\Plugin\SectionStorage\SectionStorageBase;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionListInterface;
use Drupal\layout_builder\SectionListTrait;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines the 'field_defaults' section storage type.
 *
 * @SectionStorage(
 *   id = "field_defaults",
 *   weight = 20,
 *   context_definitions = {
 *     "entity_type" = @ContextDefinition("string"),
 *     "bundle" = @ContextDefinition("string"),
 *     "field" = @ContextDefinition("entity:field_config"),
 *   },
 * )
 */
class FieldDefaultsSectionStorage extends DefaultsSectionStorage implements SectionListInterface {

  use SectionListTrait;

  /**
   * An array of sections.
   *
   * @var \Drupal\layout_builder\Section[]|null
   */
  protected ?array $sections = NULL;

  /**
   * {@inheritdoc}
   */
  public function buildRoutes(RouteCollection $collection) {
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageId() {
    return $this->getContextValue('entity_type') . '.' . $this->getContextValue('bundle') . '.' . $this->getContextValue('field')->getName();
  }

  /**
   * Gets field configuration from the current context.
   *
   * @return \Drupal\Core\Field\FieldConfigInterface
   *   The field configuration.
   */
  protected function getFieldConfig(): FieldConfigInterface {
    return $this->getContextValue('field');
  }

  /**
   * Get section configuration.
   *
   * @return array
   *   An array of sections.
   */
  protected function getSectionConfig() {
    return $this->getFieldConfig()->get('default_value') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutBuilderUrl($rel = 'view') {
    return Url::fromRoute('entity.field_config.' . $this->getContextValue('entity_type') . '_field_edit_form', [
      'node_type' => $this->getContextValue('bundle'),
      'field_config' => $this->getFieldConfig()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSections() {
    if ($this->sections !== NULL) {
      return $this->sections;
    }
    $this->sections = array_map(fn($value) => Section::fromArray($value), $this->getSectionConfig());
    return $this->sections;
  }

  /**
   * {@inheritdoc}
   */
  protected function setSections(array $sections) {
    $this->sections = array_values($sections);
  }

  /**
   * {@inheritdoc}
   */
  public function deriveContextsFromRoute($value, $definition, $name, array $defaults) {
    [$entity_type, $bundle, $field_name] = explode('.', $value);
    $field_config = FieldConfig::loadByName($entity_type, $bundle, $field_name);
    $contexts = [
      'entity_type' => new Context(new ContextDefinition('string'), $entity_type),
      'bundle' => new Context(new ContextDefinition('string'), $bundle),
      'field' => EntityContext::fromEntity($field_config),
    ];
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getContextsDuringPreview() {
    $contexts = SectionStorageBase::getContextsDuringPreview();
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIf($account->hasPermission('administer ' . $this->getContextValue('entity_type') . ' fields'))->cachePerPermissions();
    return $return_as_object ? $result : $result->isAllowed();
  }

}
