<?php

namespace Drupal\komponent;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Language\LanguageInterface;
use Drupal\layout_builder\SectionComponent;

/**
 * Clones Layout Builder sections.
 */
class CloneLayoutBuilderSections {

  /**
   * Clones sections.
   *
   * @param \Drupal\layout_builder\Section[] $source_sections
   *   The source sections being cloned.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The target language.
   */
  public function clone(array $source_sections, LanguageInterface $language) {
    $uuid = \Drupal::service('uuid');

    $sections = [];

    foreach ($source_sections as $source_section) {
      $section = clone $source_section;

      foreach ($section->getComponents() as $section_component) {
        $section->removeComponent($section_component->getUuid());
      }

      $source_components = $source_section->getComponents();
      uasort($source_components, function (SectionComponent $a, SectionComponent $b) {
        return $a->getWeight() > $b->getWeight() ? 1 : -1;
      });

      foreach ($source_components as $source_component) {
        $component = clone $source_component;
        $configuration = $component->get('configuration');

        if ($this->isInlineBlock($configuration['id'])) {
          if ($configuration['block_serialized']) {
            $block = unserialize($configuration['block_serialized'], ['allowed_classes' => [BlockContent::class]]);
          }
          else {
            $block = \Drupal::service('entity_type.manager')->getStorage('block_content')->loadRevision($configuration['block_revision_id']);
          }

          if (!$block->isNew()) {
            $block = $block->createDuplicate();

            /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions */
            $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($block->getEntityTypeId(), $block->bundle());
            foreach ($field_definitions as $definition) {

              // Support for Entity reference revisions.
              if ($definition->getFieldStorageDefinition()->getType() == 'entity_reference_revisions') {
                $new_values = [];
                $target_type = $definition->getFieldStorageDefinition()->getSetting('target_type');
                $values = $block->get($definition->getName())->getValue();
                if (!empty($values)) {
                  foreach ($values as $value) {
                    /** @var \Drupal\Core\Entity\EntityInterface $reference */
                    /** @var \Drupal\Core\Entity\EntityInterface $reference_clone */
                    $reference = \Drupal::service('entity_type.manager')->getStorage($target_type)->load($value['target_id']);
                    $reference_clone = $reference->createDuplicate();

                    $new_values[] = [
                      'entity' => $reference_clone,
                    ];
                  }

                  if (!empty($new_values)) {
                    $block->set($definition->getName(), $new_values);
                  }
                }
              }
            }

          }

          $configuration['block_revision_id'] = FALSE;
          $configuration['block_serialized'] = serialize($block);

          $component->setConfiguration($configuration);
        }

        $component->set('uuid', $uuid->generate());
        $section->appendComponent($component);
      }

      $sections[] = $section;
    }

    return $sections;
  }

  /**
   * Checks if a block ID is an inline block.
   *
   * @return bool
   *   Returns TRUE if block ID is an inline block, otherwise FALSE.
   */
  protected function isInlineBlock($block_id) {
    return substr($block_id, 0, 13) === 'inline_block:';
  }

}
